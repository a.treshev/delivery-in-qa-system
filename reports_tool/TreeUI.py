try:
    from Tkinter import *
    from ttk import *
except ImportError:  # Python 3
    from tkinter import *
    from tkinter.ttk import *


class TreeUI(Frame):
    def __init__(self, parent):
        Frame.__init__(self, parent)
        self.CreateUI()
        # self.LoadTable()
        self.grid(sticky=(N, S, W, E))
        parent.grid_rowconfigure(0, weight=1)
        parent.grid_columnconfigure(0, weight=1)
        super().__init__(self)

    def CreateUI(self):
        def treeview_sort_column(tv, col, reverse):
            if (col == -1):
                l = [(tv.item(k)["text"], k) for k in tv.get_children()]
            else:
                l = [(tv.set(k, col), k) for k in tv.get_children()]
            l.sort(reverse=reverse)
            # rearrange items in sorted positions
            for index, (val, k) in enumerate(l):
                tv.move(k, '', index)
            # reverse sort next time
            if (col == -1):
                tv.heading("#0", command=lambda: treeview_sort_column(tv, col, not reverse))
            else:
                tv.heading(col, command=lambda: treeview_sort_column(tv, col, not reverse))

        tv = Treeview(self)
        tv['columns'] = ('starttime', 'endtime', 'status')
        tv.heading("#0", text='Rev', command=lambda: treeview_sort_column(tv, -1, False))
        tv.column("#0", anchor='center', width=60, stretch=NO)

        tv.heading('starttime', text='Issue', command=lambda: treeview_sort_column(tv, 0, False))
        tv.column('starttime', anchor='w', width=120, stretch=NO)

        tv.heading('endtime', text='Status', command=lambda: treeview_sort_column(tv, 1, False))
        tv.column('endtime', anchor='w', width=120, stretch=NO)

        tv.heading('status', text='Title', command=lambda: treeview_sort_column(tv, 2, False))
        tv.column('status', anchor='w', width=600)

        tv.grid(sticky=(N, S, W, E))
        self.treeview = tv
        self.grid_rowconfigure(0, weight=1)
        self.grid_columnconfigure(0, weight=1)

    def LoadTable(self, commit_list):
        self.clear_tree()
        if commit_list:
            for commit in commit_list:
                self.treeview.insert('', 'end', text=commit.revision,
                                     values=(commit.issue, commit.issue_status, commit.issue_title))
        else:
            self.treeview.insert('', 'end', text="ERROR: Not correct credentials to JIRA server please check user and password")


    def clear_tree(self):
        self.treeview.delete(*self.treeview.get_children())

    def destroy(self):
        pass
