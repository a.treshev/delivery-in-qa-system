import configparser
import os

import ui


def sort_by_issue_number(object):
    if (object.issue is None): return ""
    return object.issue


def sort_by_issue_priority(object):
    return str(object.get_priority()), str(object.issue_title)


def remove_duplicates_by_issue_name(source_list):
    source_list.sort(key=sort_by_issue_priority)
    output_list = []
    tmp_object = None
    for final in source_list:
        if final.issue is None or tmp_object != final.issue:
            output_list.append(final)
        tmp_object = final.issue
    # output_list.sort(key=sortByRevision, reverse=True)
    return output_list


def print_data(list_with_commits_data):
    for data in list_with_commits_data:
        print("{revision} {issue} <{status}> {assignee} {title}".format(revision=data.revision, issue=data.issue,
                                                                        status=data.issue_status,
                                                                        assignee=data.issue_assignee,
                                                                        title=data.issue_title))


def get_print_data(list_with_commits_data):
    output = ""
    for data in list_with_commits_data:
        output += (
            "{revision} {issue:14s} <{status}> {title}\n".format(revision=data.revision,
                                                                 issue=str(data.issue),
                                                                 status=str(data.issue_status),
                                                                 title=data.issue_title))
    return output


def get_build_file_name():
    config = configparser.ConfigParser()
    config.read(ui.config_file_name)
    ftp_folder = config["mail"]["ftp_folder"]
    if ftp_folder:
        for res in os.scandir(ftp_folder):
            if (config["updated"]["last_processing_revision"] in res.name):
                return (res.path)
            return ("Has not been built yet")
    return "<font color='{}'><b>{}</b></font>".format("red", "Please setup FTP folder in config file");
