import configparser
import datetime
import re

import svn.exception
import svn.remote

from reports_tool.commit import Commit
import ui


class SVNObject:
    def __init__(self):
        self._config = configparser.ConfigParser()
        self._config.read(ui.config_file_name)
        self._main_branch_name = self._config["svn"]["main_branch"]
        self._merged_branch_name = self._config["svn"]["merged_branch"]
        self._data_list = []

    def get_commits_from_the_revision(self, start_revision: int):
        svn_data_list = self.get_svn_data_from_revision(start_revision)
        commit_info_list = self.parse_loaded_svn_date_into_commit_object(svn_data_list)
        commit_info_list.sort(key=self.sort_by_revision, reverse=True)
        return commit_info_list

    def get_svn_data_from_revision(self, start_revision: int):
        main_branch = svn.remote.RemoteClient(self._main_branch_name)

        data_from_svn = list(main_branch.log_default(revision_from=start_revision))

        # if 'None' not in self._merged_branch_name:
        #     merged_branch = svn.remote.RemoteClient(self._merged_branch_name)
        #     data_from_svn.append(list(merged_branch.log_default()))

        return data_from_svn

    @staticmethod
    def update_config_file(list_with_commits_data):
        if len(list_with_commits_data) > 0:
            last_revision = list_with_commits_data[0].revision

            config_file = configparser.ConfigParser()
            config_file.read(ui.config_file_name)

            # write info to the config file
            config_file["updated"]["last_processing_time"] = datetime.datetime.now().strftime("%d.%m.%Y %H:%M")
            config_file["updated"]["last_processing_revision"] = str(last_revision)

            with open('config2.ini', 'w') as configfile:
                config_file.write(configfile)

        return list_with_commits_data

    @staticmethod
    def sort_by_revision(commit_object):
        return commit_object.revision

    @staticmethod
    def parse_loaded_svn_date_into_commit_object(svn_data_list):
        result_commit_objects_list = []
        for l in svn_data_list:
            try:
                print(l)
                res = re.search('[A-Z]+-[0-9]+', str(l.msg))
                commit = Commit(None)
                commit.revision = l.revision
                commit.message = l.msg
                commit.author = l.author
                if res is not None:
                    message = res.group()
                    commit.issue = message
                    commit.message = l.msg[len(message) + 1:]
                else:
                    commit.issue = None
                    commit.issue_title = str(l.msg)
                result_commit_objects_list.append(commit)
            except Exception as e:
                print("Error in SVN ", e)
        return result_commit_objects_list


def get_commits_from_revision(start_revision: int):
    svn_object = SVNObject()
    try:
        result_list = svn_object.get_commits_from_the_revision(start_revision)
        svn_object.update_config_file(result_list)
        return result_list
    except Exception as e:
        print("\n### ### ERROR ### ###")
        print("Error during SVN connection. Please check credentials and connection string")
        print(e)
        print("### ### ### ### \n")
    return None


if __name__ == "__main__":
    svn_obj = SVNObject()
