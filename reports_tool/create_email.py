import configparser
import ui
import win32com.client as win32


import reports_tool.utils as utils
from reports_tool.commit import Commit

outlook = win32.Dispatch("Outlook.Application")
ns = outlook.GetNamespace("MAPI")


def emailer(content, subject, to, cc=None, fwd_message=None):
    if fwd_message is None:
        mail = outlook.CreateItem(0)
    else:
        mail = fwd_message.forward
    mail.To = to
    mail.Subject = subject
    mail.CC = cc
    mail.GetInspector
    index = mail.HTMLbody.find('>', mail.HTMLbody.find('<body'))
    mail.HTMLbody = mail.HTMLbody[:index + 1] + content + mail.HTMLbody[index + 1:]
    mail.HTMLbody = mail.HTMLbody.replace('SimSun', 'Calibri (Body)')
    mail.Display()


def remote_dubles(data_list):
    data_list.sort(key=utils.sort_by_issue_number)
    data_list = utils.remove_duplicates_by_issue_name(data_list)
    data_list.sort(key=utils.sort_by_issue_priority)
    return data_list


def find_last_sent_message():
    config = configparser.ConfigParser()
    config.read(ui.config_file_name)

    # string should be like '[DT SMF 17.3] PPS CD2 TOMS QA Patch'
    find_subject_string = str(config["mail"]["subject"])[:-6]
    # equals to find by SUBJECT
    filter = "@SQL=""http://schemas.microsoft.com/mapi/proptag/0x0037001f"" ci_phrasematch '" + find_subject_string + "'"

    inbox = ns.GetDefaultFolder(6)
    messages = inbox.Items.restrict(filter)
    messages.Sort("[ReceivedTime]", True)
    if len(messages) > 0: return messages[0]

    return None


def create_email(data_list):
    config = configparser.ConfigParser()
    config.read(ui.config_file_name)

    data_list = remote_dubles(data_list)
    build_file_name = utils.get_build_file_name()
    content = Commit._START_TABLE
    i = 1
    for output in data_list:
        if (output.issue is not None):
            content += output.get_html_content(i)
            i += 1
    content += Commit._END_TABLE

    content = config["mail"]["body_text"].format(build_file_name, content)
    emailer(content, config["mail"]["subject"].format(config["updated"]["last_processing_revision"]),
            config["mail"]["to"], config["mail"]["cc"], find_last_sent_message())
