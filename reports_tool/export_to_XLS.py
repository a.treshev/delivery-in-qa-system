import win32com.client as win32

def export(commit_list):
    excel = win32.gencache.EnsureDispatch('Excel.Application')
    wb = excel.Workbooks.Add()

    try:
        sheet = wb.ActiveSheet
        # sheet = wb.Worksheets("CommitReport")

        #записываем Header
        sheet.Cells(1,1).Value = "Rev"
        sheet.Cells(1,2).Value = "Author"
        sheet.Cells(1,3).Value = "Message:"
        sheet.Cells(1,4).Value = "Comment"
        sheet.Cells(1,5).Value = "TMS summary"
        sheet.Cells(1,6).Value = "Status"
        sheet.Cells(1,7).Value = "Comment2"

        sheet.Range("A1:G1").Font.Bold = True
        sheet.Range("A1:G1").AutoFilter(1)
        # sheet.Columns(1).AutoFilter(1)

        #записываем последовательность
        for j, commit in enumerate(commit_list):
            sheet.Cells(j+2,1).Value = commit.revision
            sheet.Cells(j+2,2).Value = str(commit.author)
            sheet.Cells(j+2,3).Value = commit.issue if commit.issue is not None else 'None'
            sheet.Cells(j+2,4).Value = commit.message
            sheet.Cells(j+2,5).Value = commit.issue_title
            sheet.Cells(j+2,6).Value = str(commit.issue_status)

        sheet.Columns.AutoFit()
        sheet.Rows.AutoFit()

        wb.SaveAs('UpdatedSheet.xlsx')

        #закрываем ее

    finally:
        # закрываем COM объект
        wb.Close()
        excel.Quit()
