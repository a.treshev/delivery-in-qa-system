import configparser

import ui
from jira import JIRA, exceptions



class JIRAObject:
    def __init__(self):
        options = {
            'server': 'https://tms.netcracker.com'}
        credentials = self.load_credentials()
        try:
            self._jira = JIRA(options, basic_auth=(credentials["user"], credentials["password"]))
        except exceptions.JIRAError as er:
            print("\n### ### ERROR ### ###")
            print("JIRA Error: Not correct credentials to JIRA server please check user and password")
            # print(er) -- return the HTML from JIRA server
            print("### ### ### ### \n")

    def load_credentials(self):
        config = configparser.ConfigParser()
        config.read(ui.config_file_name)
        user = config["jira"]["user"]
        password = config["jira"]["password"]
        return {"user": user, "password": password}

    def load_data_from_JIRA(self, commit_list):
        if "_jira" in self.__dict__:
            jira = self._jira

            for commit in commit_list:
                if commit.issue is not None:
                    issue = jira.issue(commit.issue)
                    commit.issue_title = issue.fields.summary
                    commit.issue_status = issue.fields.status
                    commit.issue_assignee = issue.fields.assignee
                    if issue.fields.fixVersions is not None and len(issue.fields.fixVersions)>0:
                        commit.issue_fix_revision = issue.fields.fixVersions[0]
            return commit_list
        else:
            return None


def load_data_from_JIRA(commit_list):
    return JIRAObject().load_data_from_JIRA(commit_list)


if __name__ == "__main__":
    jira_object = JIRAObject()
    print(jira_object)
