class Commit():
    def __init__(self, commit_string, revision=None, author=None, message=None, issue=None, issue_url=None):
        if (commit_string is not None):
            self.revision = commit_string[0].strip().replace("Revision: ", "")
            self.author = commit_string[1].strip().replace("Author: ", "")
            self.message = commit_string[4].strip()+" "+commit_string[5].strip()
            if "RE" not in commit_string[4].strip():
                self.issue = commit_string[4].split( )[0].strip(',')
            else:
                self.issue = None

        self.author = None
        self.issue = None
        self.issue_title = None
        self.issue_status = None
        self.issue_assignee = None
        self.issue_fix_revision = None

    def get_issue_url(self):
        if (self.issue is not None):
            return "https://tms.netcracker.com/browse/" + self.issue
        return None

    def get_field_name(name):
        dict_property_to_string = {
            "issue":"Issue Number",
            "issue_title":"Issue Title",
            "issue_status":"Issue Status",
            "issue_assignee":"assignee",
        }
        return dict_property_to_string[name]


    def get_color(self):
        dict_status_to_color = {
            'Closed': "green",
            'Resolved': "green",
            'Ready for Testing': "blue",
            'Reopened': "red",
            'Implemented': "none",
            'In Progress': "red",
            'Additional Info Required': "red"
        }
        try:
            return dict_status_to_color[str(self.issue_status)]
        except KeyError as e:
            return "none"

    def get_priority(self):
        dict_status_to_prio = {
            'Closed': "6",
            'Resolved': "5",
            'Ready for Testing': "4",
            'Reopened': "2",
            'Implemented': "3",
            'In Progress': "1",
            'Additional Info Required': "1"
        }
        try:
            return dict_status_to_prio[str(self.issue_status)]
        except KeyError as e:
            return 10

    _START_TABLE = "<table border><th>#</th><th style='text-align:left; width:130px'>Issue</th><th style='width: 130px'>Status</th><th style='width: 130px'>Assignee</th><th style='text-align:left'>Title</th><th style='text-align:left'>Fix version</th>"
    _END_TABLE = "</table>"

    def get_html_content(self, index):
        string = "<tr>"
        #index
        string += "<td><b>{:2n}</b></td>".format(index)
        #issue number and url
        string += "<td><a href='{}'>{}</a></td>".format(self.get_issue_url(), self.issue)
        #issue status
        string += "<td text-align: center;'><font color='{}'><b>{}</b></font></td>".format(self.get_color(), self.issue_status)
        #issue issue_assignee
        string += "<td>{}</td>".format(self.issue_assignee)
        #issue title
        string += "<td><b>{}</b></td>".format(self.issue_title)
        # issue fix versions
        string += "<td><b>{}</b></td>".format(self.issue_fix_revision)
        string += "</tr>"
        return string