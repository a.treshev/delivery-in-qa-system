import configparser
import datetime
import threading
from tkinter import *

from reports_tool import TreeUI
from reports_tool import create_email
from reports_tool import export_to_XLS
from reports_tool import get_JIRA_data
from reports_tool import get_svn_data

config = configparser.ConfigParser()
global_list_with_commits_data = []
config_file_name = 'config2.ini'

if __name__ == '__main__':
    def get_initial_list():
        config.read(config_file_name)
        last_data = config["tmp"]["last_data"]
        return last_data


    def run_button_on_click():
        config.read(config_file_name)
        # text_filed.delete('1.0', END)

        start_revision = revision_input.get()
        if not start_revision.isdigit() or int(start_revision) < 19500:
            start_revision = config["DEFAULT"]["revision_start"]

        start_revision = int(start_revision)
        start_revision_text = "Start revision is: {}\n".format(start_revision)
        status_line.config(text=start_revision_text)

        get_commits_information = threading.Thread(target=set_commits_info_in_text_field, args=[start_revision])
        get_commits_information.start()


    def set_commits_info_in_text_field(start_revision):
        start_date = datetime.datetime.now()
        print("Start SVN call")
        list_with_commits_data = get_svn_data.get_commits_from_revision(start_revision)
        svn_string = "SVN request time: {} {}".format((datetime.datetime.now() - start_date).seconds, 'sec')
        print(svn_string)
        status_line.config(text=svn_string)

        if (list_with_commits_data):
            print("Start JIRA call")
            list_with_commits_data = get_JIRA_data.load_data_from_JIRA(list_with_commits_data)
            jira_string = "JIRA request time: {} {}".format((datetime.datetime.now() - start_date).seconds, 'sec')
            print(jira_string)
            status_line.config(text=jira_string)

            treeVew.LoadTable(list_with_commits_data)

            if list_with_commits_data and len(list_with_commits_data) != 0:
                global global_list_with_commits_data
                global_list_with_commits_data = list_with_commits_data


    def mail_button_on_click():
        config.read(config_file_name)
        create_email.create_email(global_list_with_commits_data)

        if len(global_list_with_commits_data) > 0:
            last_revision = global_list_with_commits_data[0].revision
            config["DEFAULT"]["last_qua"] = str(last_revision)

        with open(config_file_name, 'w') as configfile:
            config.write(configfile)


    def expot_to_XLS_on_click():
        if len(global_list_with_commits_data) > 0:
            export_to_XLS.export(global_list_with_commits_data)
            # pass
        else:
            toplevel = Toplevel()
            label1 = Label(toplevel, text="Списк коммитов пуст \n Сначала запустите выгрузку", height=0, width=30)
            label1.pack()


    # GUI BLOCK
    config.read(config_file_name)
    root = Tk(baseName="Commit report")

    initial_block = Label(root, fg="white")
    # initial_block = Label(root, fg="white", bg="red")
    initial_block.pack(fill=X)

    # DATA BLOCK START
    data_block = Label(initial_block, fg="white")
    data_block.pack(side="left")

    Label(data_block, text="Start revision:").grid(sticky="W", row=0)
    revision_input = Entry(data_block)
    revision_input.insert(END, config["DEFAULT"]["last_qua"])
    revision_input.grid(row=0, column=1)

    Label(data_block, text="Last QA revision: ").grid(sticky="W", row=1)
    qa_label = Label(data_block, text="test")
    qa_label.grid(sticky="W", row=1, column=1)
    qa_label.config(text=str(config["DEFAULT"]["last_qua"]))

    Label(data_block, text="Last build revision: ").grid(sticky="W", row=2)
    last_label = Label(data_block, text="test")
    last_label.grid(sticky="W", row=2, column=1)
    last_label.config(text=str(config["DEFAULT"]["previous_delivery"]))

    # SEND BUTTON BLOCK
    send_buttons_block = Label(initial_block, fg="white")
    send_buttons_block.pack(after=data_block, side="left")

    button_run = Button(send_buttons_block, text='Run', width=25, command=run_button_on_click)
    button_run.pack()

    # BUTTONS BLOCK START
    buttons_block = Label(initial_block, fg="white")
    buttons_block.pack(after=send_buttons_block, side="right")

    button_run = Button(buttons_block, text='Export to XLS', width=25, command=expot_to_XLS_on_click)
    button_run.pack()

    button_send = Button(buttons_block, text='Send Email', width=25, command=mail_button_on_click)
    button_send.pack()

    # TREE BLOCK START
    text_block = Label(root, fg="white")
    # text_block.pack(fill=Y)
    text_block.pack(expand=True, fill='both')
    treeVew = TreeUI.TreeUI(text_block)

    status_line = Label(root, text="Status: ")
    status_line.pack(fill='both', side="left", expand=False)

    mainloop()
